# Tenet grammar autobuild

This uses bnfc to build the grammar.

The haskell grammar is built twice, once under `build` to run quick tests, once again to package it with Stack. A LaTeX doc is also produced under `build`.

## Build functions

A full build can be done with `./build.sh all`.

Subcommands include:

 * `c` builds a c parser in `work/c`
 * `bison` builds the c parser and then runs `bison` against it to create counter-examples for conflicts
 * `haskell` builds a haskell parser in `work/haskell` for running tests
 * `stack` updates the haskell parser into `tenet-grammar` for inclusion via stack.
 * `latex` builds the LaTeX documentation of the syntax rules in `work/latex`.
 * `site` builds the LaTeX documentation converting it for use in the website.
