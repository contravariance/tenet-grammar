entrypoints Module, Pragma, Statement, TopStmt, TypeExpr, ValExpr ;

MModule.       Module ::= [Pragma] [Statement] ;

-- Used for test parsing.
TSPragma.      TopStmt ::= Pragma ;
TSStmt.        TopStmt ::= Statement ;

PTenetVersion. Pragma ::= "tenet" Integer ;
PMeta.         Pragma ::= "meta" PragmaWord "(" [PragmaArg] ")" ;
PGen.          Pragma ::= "gen" [GenName] "{" [GenConfig] "}" ;
PImportOne.    Pragma ::= "import" [ModName] "." NameSpec ;
PImportMany.   Pragma ::= "import" [ModName] "(" [NameSpec] ")" ;
separator Pragma "" ;

_.             Pragma ::= Pragma ";" ;

PAInt.         PragmaArg ::= Integer ;
PAStr.         PragmaArg ::= String ;
PAValId.       PragmaArg ::= ValId ;
PaTypId.       PragmaArg ::= TypId ;
separator PragmaArg "," ;

GCSimple.      GenConfig ::= PragmaWord ;
GCComplex.     GenConfig ::= PragmaWord "(" [PragmaArg] ")" ;
separator GenConfig "" ;

NSValSimple.   NameSpec ::= ValId ;
NSValRename.   NameSpec ::= ValId "as" ValId ;
NSTypSimple.   NameSpec ::= TypId ;
NSTypRename.   NameSpec ::= TypId "as" TypId ;
separator nonempty NameSpec "," ;

STypeDef.      Statement ::= "type" TypeLhsExpr AssignOp TypeExpr ;
SDef.          Statement ::= "def" LetLhsExpr AssignOp ValExpr ;
SLet.          Statement ::= "let" LetLhsExpr AugAssignOp ValExpr ;
SBare.         Statement ::= LetLhsExpr AugAssignOp ValExpr ;
SFunc.         Statement ::= "func" ValId "(" [SigArg] ")" SigReturn Block ;
SIf.           Statement ::= "if" ValExpr Block ElseClause ;
SSwitch.       Statement ::= "switch" ValExpr CaseBlock ;
STry.          Statement ::= "try" Block [CatchClause] ;
SFor.          Statement ::= "for" ValId "in" ValExpr Block ;
SReturn.       Statement ::= "return" ValExpr ;
SPass.         Statement ::= "pass" ;
SContinue.     Statement ::= "continue" ;
SBreak.        Statement ::= "break" ;
separator nonempty Statement "" ;

_.             Statement ::= Statement ";" ;

SASimple.      SigArg ::= ValId ;
SAAnnotate.    SigArg ::= ValId ":" TypeExpr ;
SAGeneric.     SigArg ::= TypId ;
separator SigArg "," ;

SREmpty.       SigReturn ::= ;
SRArrow.       SigReturn ::= "->" TypeExpr ;

BBlock.        Block ::= "{" [Statement] "}" ;

ECEmpty.       ElseClause ::= ;
ECElse.        ElseClause ::= "else" Block ;
ECElseIf.      ElseClause ::= "else" "if" ValExpr Block ElseClause;

CBCaseBlock.   CaseBlock ::= "{" [CaseClause] "}" ;

CCCase.        CaseClause ::= "case" PatternExpr ":" [Statement] ;
CCDefault.     CaseClause ::= "default" ":" [Statement] ;
separator CaseClause "" ;

CCatchClause.  CatchClause ::= "catch" [Exception] Block ;
separator nonempty CatchClause "" ;

EException.    Exception ::= TypId ;
separator nonempty Exception "," ;

PEEnum.        PatternExpr ::= "#" ValId ;
PEUnion.       PatternExpr ::= ValId "~" PatternExpr ;
PEName.        PatternExpr ::= ValId ;
PEInteger.     PatternExpr ::= Integer ;
PEPosInteger.  PatternExpr ::= "+" Integer ;
PENegInteger.  PatternExpr ::= "-" Integer ;
PEString.      PatternExpr ::= String ;
PERecord.      PatternExpr ::= "(" [SlotPatternExpr] ")" ;
PEAny.         PatternExpr ::= "*";
PEBoolTrue.    PatternExpr ::= "true";
PEBoolFalse.   PatternExpr ::= "false";

SPEAbbrev.     SlotPatternExpr ::= ValId ":" ;
SPEFull.       SlotPatternExpr ::= ValId ":" PatternExpr ;
separator SlotPatternExpr "," ;

VEGuard.       ValExpr ::= ValExpr1 "|" ValExpr ;
VELogXor.      ValExpr1 ::= ValExpr1 "xor" ValExpr2 ;
VELogEqv.      ValExpr1 ::= ValExpr1 "eqv" ValExpr2 ;
VELogImp.      ValExpr1 ::= ValExpr1 "imp" ValExpr2 ;
VELogOr.       ValExpr2 ::= ValExpr2 "or" ValExpr3 ;
VELogAnd.      ValExpr3 ::= ValExpr3 "and" ValExpr4 ;
VELogNot.      ValExpr4 ::= "not" ValExpr5 ;
VERelEq.       ValExpr5 ::= ValExpr5 "==" ValExpr6 ;
VERelNotEq.    ValExpr5 ::= ValExpr5 "!=" ValExpr6 ;
VERelLess.     ValExpr5 ::= ValExpr5 "<" ValExpr6 ;
VERelLessEq.   ValExpr5 ::= ValExpr5 "<=" ValExpr6 ;
VERelGrt.      ValExpr5 ::= ValExpr5 ">" ValExpr6 ;
VERelGrtEq.    ValExpr5 ::= ValExpr5 ">=" ValExpr6 ;
VERelIn.       ValExpr5 ::= ValExpr5 "in" ValExpr6 ;
VERelNotIn.    ValExpr5 ::= ValExpr5 "not" "in" ValExpr6 ;
VEAddPlus.     ValExpr6 ::= ValExpr6 "+" ValExpr7 ;
VEAddMinus.    ValExpr6 ::= ValExpr6 "-" ValExpr7 ;
VEAddConcat.   ValExpr6 ::= ValExpr6 "++" ValExpr7 ;
VEAddUnion.    ValExpr6 ::= ValExpr6 "||" ValExpr7 ;
VEAddDiff.     ValExpr6 ::= ValExpr6 "--" ValExpr7 ;
VEMulTimes.    ValExpr7 ::= ValExpr7 "*" ValExpr8 ;
VEMulDivide.   ValExpr7 ::= ValExpr7 "/" ValExpr8 ;
VEMulFloorDiv. ValExpr7 ::= ValExpr7 "//" ValExpr8 ;
VEMulModulo.   ValExpr7 ::= ValExpr7 "%" ValExpr8 ;
VEMulInter.    ValExpr7 ::= ValExpr7 "&&" ValExpr8 ;
VEConUnion.    ValExpr8 ::= ValId "~" ValExpr8 ;
VEApply.       ValExpr9 ::= ValExpr9 "(" [AppArg] ")" ;
VEPartApply.   ValExpr9 ::= ValExpr9 "(" [AppArg] "..." ")" ;
VEIndex.       ValExpr9 ::= ValExpr9 "[" ValExpr "]" ;
VEIndexSlice.  ValExpr9 ::= ValExpr9 "[" ValExpr ".." ValExpr "]" ;
VEParamSlot.   ValExpr9 ::= ValExpr9 "." ValId ;
VEParamVar.    ValExpr9 ::= ValExpr9 "?" ValId ;
VEUnaryPlus.   ValExpr10 ::= "+" ValExpr10 ;
VEUnaryMinus.  ValExpr10 ::= "-" ValExpr10 ;
VEName.        ValExpr11 ::= ValId ;
VELitInt.      ValExpr11 ::= Integer ;
VELitString.   ValExpr11 ::= String ;
VELitFalse.    ValExpr11 ::= "false" ;
VELitTrue.     ValExpr11 ::= "true" ;
VESpecial.     ValExpr11 ::= "!!" ValId "(" [ValExpr] ")" ;
VELitList.     ValExpr11 ::= "[" [ValExpr] "]" ;
VELitSet.      ValExpr11 ::= "{" [ValExpr] "}" ;
VELitMapEmpty. ValExpr11 ::= "{" ":" "}" ;
VELitMap.      ValExpr11 ::= "{" [MapPair] "}" ;
VELitEnum.     ValExpr11 ::= "#" ValId ;
VELitRecord.   ValExpr11 ::= "(" [RecPair] ")" ;
VELazyExpr.    ValExpr11 ::= "#(" ValExpr ")" ;
VELambda.      ValExpr11 ::= "func" "(" [SigArg] ")" SigReturn Block ;

coercions ValExpr 11 ;
separator ValExpr "," ;

-- This accepts invalid use of positional arguments; those are caught
-- in desugaring.
AAPos.         AppArg ::= ValExpr ;
AAKeyword.     AppArg ::= ValId ":" ValExpr ;
AAKwShort.     AppArg ::= ValId ":" ;
AAType.        AppArg ::= TypId ":" TypeExpr ;

separator AppArg "," ;

TPTypeParam.   TypeParam ::= TypId ":" TypeExpr ;
separator TypeParam "," ;

MPMapPair.     MapPair ::= ValExpr ":" ValExpr ;
(:[]).         [MapPair] ::= MapPair ;
(:[]).         [MapPair] ::= MapPair "," ;
(:).           [MapPair] ::= MapPair "," [MapPair] ;

RPShort.       RecPair ::= ValId ":" ;
RPNormal.      RecPair ::= ValId ":" ValExpr ;
separator RecPair "," ;

TEFunc.        TypeExpr  ::= "Func" "(" [SigArg] ")" "->" TypeExpr1 ;
TEUnion.       TypeExpr1 ::= TypeExpr1 "|" TypeExpr2 ;
TEVariant.     TypeExpr2 ::= ValId "~" TypeExpr2 ;
TEList.        TypeExpr3 ::= "[" TypeExpr "]" ;
TESet.         TypeExpr3 ::= "{" TypeExpr "}" ;
TEMap.         TypeExpr3 ::= "{" TypeExpr ":" TypeExpr "}" ;
TERecord.      TypeExpr3 ::= "(" [RecParam] ")" ;
TEEnum.        TypeExpr4 ::= "#" ValId ;
TENameParams.  TypeExpr4 ::= TypId "[" [TypeParam] "]" ;
TEName.        TypeExpr4 ::= TypId ;
TEUnspecified. TypeExpr4 ::= "!Unspec" ;
coercions TypeExpr 4 ;

RPRecParam.    RecParam ::= ValId ":" TypeExpr ;
separator RecParam "," ;

TLName.        TypeLhsExpr ::= TypId ;
TLParams.      TypeLhsExpr ::= TypId "[" [TypId] "]" ;

LLTyped.       LetLhsExpr ::= ValId ":" TypeExpr ;
LLLens.        LetLhsExpr ::= ValId [LensExpr] ;
LLDestruct.    LetLhsExpr ::= "(" [DestructurePhrase] ")" ;

LEVariant.     LensExpr ::= "?" ValId ;
LESlot.        LensExpr ::= "." ValId ;
LEIndex.       LensExpr ::= "[" ValExpr "]" ;
LESlice.       LensExpr ::= "[" ValExpr ".." ValExpr "]" ;
separator LensExpr "" ;

DPSimple.      DestructurePhrase ::= ValId ;
DPRename.      DestructurePhrase ::= ValId ":" ValId [LensExpr] ;
DPRecord.      DestructurePhrase ::= ValId ":" "(" [DestructurePhrase] ")" ;
separator DestructurePhrase "," ;

AAssign.       AssignOp ::= ":=" ;
AAssignC.      AssignOp ::= "=" ;

AOAssign.      AugAssignOp ::= ":=" ;
AOAssignC.     AugAssignOp ::= "=" ;
AOAdd.         AugAssignOp ::= "+=" ;
AOMinus.       AugAssignOp ::= "-=" ;
AOTimes.       AugAssignOp ::= "*=" ;
AODivide.      AugAssignOp ::= "/=" ;
AOFloorDiv.    AugAssignOp ::= "//=" ;
AOModulo.      AugAssignOp ::= "%=" ;
AOConcat.      AugAssignOp ::= "++=" ;
AOUnion.       AugAssignOp ::= "||=" ;
AOInter.       AugAssignOp ::= "&&=" ;
AODiff.        AugAssignOp ::= "--=" ;

comment ";;" ;

-- We will validate identifiers according to the strict rules within error scanning.
-- Hopefully this makes it simpler to write fixers.
token ValId (lower (letter | digit | '_')*) ;
token TypId (upper (letter | digit | '_')*) ;
separator nonempty TypId "," ;

-- Some "tokens" are really rules.
TModName.     ModName ::= ValId ;
separator nonempty ModName "." ;

TPragmaWord.  PragmaWord ::= ValId ;
TGenName.     GenName ::= TypId ;
separator nonempty GenName "," ;
