#!/usr/bin/env python3

import re
from html.entities import codepoint2name

word_re = re.compile(r'\w+')
symb_token_re = re.compile(r'%token(?:<\w+>)? (_SYMB_\d+) +/\* *([^ ]+) *\*/')
symb_re = re.compile(r'\b_SYMB_\d+\b')
is_word_re = re.compile(r'\w+$')
non_wordchar_re = re.compile(r'\W')

punct = {' ': '', '!': 'bang', '#': 'sharp', '%': 'pct', '(': 'lpar', ')': 'rpar', '*': 'star', '+': 'plus', ',': 'comma',
         '-': 'dash', '.': 'dot', '/': 'slash', ':': 'colon', ';': 'semi', '=': 'eq', '?': 'qst', '[': 'lbrk',
         ']': 'rbrk', '{': 'lbrc', '|': 'pipe', '}': 'rbrc', '~': 'tilde'}

no_entity = set()

def find_name(suggest, words):
    count = 0
    name = suggest
    while name in words:
        count += 1
        name = f'{suggest}_{count}'
    words.add(name)
    return name

def desym(match):
    v = match.group()
    entity = punct.get(v, codepoint2name.get(ord(v)))
    if not entity:
        no_entity.add(v)
        return ''
    return non_wordchar_re.sub('', entity.lower())

def pretty_names(in_fh, out_fh):
    lines = list(in_fh)

    words = set()
    for line in lines:
        for word in word_re.finditer(line):
            words.add(word)

    subs = {}

    for line in lines:
        if m := symb_token_re.match(line):
            sym = m.group(1)
            real = m.group(2)
            if is_word_re.match(real):
                real = 'kw_' + real
            else:
                real = non_wordchar_re.sub(desym, real)
            if real:
                subs[sym] = find_name(real, words)

    def lookup(match):
        symb = match.group(0)
        return subs.get(symb, symb)

    for line in lines:
        out_fh.write(symb_re.sub(lookup, line))


if __name__ == '__main__':
    import sys

    with open(sys.argv[1], 'r') as in_fh:
        with open(sys.argv[2], 'w') as out_fh:
            pretty_names(in_fh, out_fh)

    if no_entity:
        missing = {c: '' for c in sorted(no_entity)}
        print(f"NO ENTITY: {missing}")
