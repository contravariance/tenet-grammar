#!/bin/zsh

work() {
    echo -n work/"$1"
}

pre() {
    echo
    echo "BUILDING FOR $1"
    DIR=$(work $1)
    mkdir -p $DIR
}

run() (
    [[ -n $DIR ]] && cd $DIR && "$@"
)

build-stack() {
    echo
    echo "BUILDING FOR stack"
    pwd
    tgl=tenet-grammar/library
    tgltg=$tgl/Tenet/Grammar
    bnfc -o $tgl --haskell -p Tenet.Grammar --ghc --functor --bytestrings Tenet.cf
    # txt2tags -i $tgltg/DocTenet.txt -t html -o tenet-grammar/README.html
    rm $tgltg/{DocTenet.txt,PrintTenet.hs,SkelTenet.hs,TestTenet.hs,*.bak}
    awk -i inplace -f fix-lex.awk $tgltg/LexTenet.x
    awk -i inplace -f fix-par.awk $tgltg/ParTenet.y
    (
        cd tenet-grammar
        stack build
    )
}

build-haskell() {
    pre haskell
    (cd $DIR && make distclean)
    bnfc -m -o $DIR --haskell --ghc --functor Tenet.cf
    run make all
    # cp $DIR/ParTenet.info ./
}

build-latex() {
    pre latex
    (cd $DIR && { rm *.bak; make clean; })
    bnfc -m --latex -o $DIR Tenet.cf
    run make all
}

build-site() {
    pre site
    cp $(work latex)/Tenet.tex $DIR/
    run make4ht Tenet.tex "mathml,mathjax"
}

build-c() {
    pre c
    (cd $DIR && make distclean)
    bnfc -m -o $DIR --c Tenet.cf
    run make all
}

build-bison() {
    pre bison
    # Prettify the symbols for readable warnings.
    python3 bison_symbols.py $(work c)/Tenet.y $DIR/Tenet.y
    # We run this for the counterexamples.
    run bison -Wcex Tenet.y
}

# Utilities called directly by this script.
# awk() {command awk "$@";}
bison() {command /usr/local/opt/bison/bin/bison "$@";}
# bnfc() {command bnfc "$@";}
# make() {command make "$@";}
# make4ht() {command make4ht "$@";}
# python3() {command python3 "$@";}
# stack() {command stack "$@";}

# Called indirectly:
# gcc, flex, bison
# ghc, happy, alex
# pdflatex

case $1 in
    stack ) build-stack ;;
    haskell ) build-haskell ;;
    latex ) build-latex ;;
    site )
        build-latex
        build-site
        ;;
    c ) build-c ;;
    bison)
        build-c
        build-bison
        ;;
    all )
        build-stack
        build-haskell
        build-latex
        build-site
        build-c
        build-bison
        ;;
    * )
        echo "Specify what to build."
        ;;
esac
