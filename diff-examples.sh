#!/bin/zsh

main() {
    remote=$1 local=$2

    for name in examples/**/*.tenet; do
        if [[ ! -e $remote/$name ]]; then
            echo "$name missing in remote"
        elif ! diff -q "$name" "$remote/$name" > /dev/null; then
            echo "==================== $name ===================="
            diff "$name" "$remote/$name"
        fi
    done

    (
        cd $remote

        for name in examples/**/*.tenet; do
            if [[ ! -e $local/$name ]]; then
                echo "$name missing in local"
            fi
        done
    )
}

main ../tenet-haskell ../tenet-grammar
