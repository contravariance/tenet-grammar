BEGIN {
    beforeImports = 1
}

beforeImports && /^import/ {
    beforeImports = 0
    print "import Data.Convertible.Utf8 (convert)"
}

/^ +\{ tok / {
    # { tok (\p s -> PT p (eitherResIdent (TV . share) s)) }
    # { tok (\p s -> PT p (eitherResIdent (TV . share) (convert s))) }
    # or
    # { tok (\p s -> PT p (TL $ share $ unescapeInitTail s)) }
    # { tok (\p s -> PT p (TL $ share $ unescapeInitTail $ convert s)) }
    # or
    # { tok (\p s -> PT p (TI $ share s))    }
    # { tok (\p s -> PT p (TI $ share $ convert s))    }
    sub(/\) s\)\) \}/, ") (convert s))) }") || sub(/ s\)\) \}/, " $ convert s)) }") || sub(/share s\)\)/, "share $ convert s))")
}

/^ +PT/ {
    # PT _ (TS s _) -> s
    # PT _ (TS s _) -> convert s
    sub(/ -> s\>/, " -> convert s")
}

/AlexToken/ && /BS\.take/ {
    # AlexToken inp' len act -> act pos (BS.take len str) : (go inp')
    # AlexToken inp' len act -> act pos (convert $ BS.take len str) : (go inp')
    sub(/\(BS\.take len str\)/, "(convert $ BS.take len str)")
}

{
    print
}
