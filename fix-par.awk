BEGIN {
    beforeImports = 1
}

beforeImports && /^import/ {
    beforeImports = 0
    print "import Data.Convertible.Utf8 (convert)"
}

/BS.unpack\(prToken t\)/ {
    sub(/BS.unpack\(prToken t\)/, "convert (prToken t)")
}

{
    print
}
