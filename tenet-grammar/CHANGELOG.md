# Change log

tenet-grammar (probably) uses [Semantic Versioning][].
The change log is available through the [releases on GitLab][].

[Semantic Versioning]: http://semver.org/spec/v2.0.0.html
[releases on GitHub]: https://gitlab.com/contravariance/tenet-grammar/releases
