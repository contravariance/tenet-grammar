#!/bin/zsh

main() {
    find examples -name \*.tenet -type f -exec work/haskell/TestTenet -s {} \;
}

main
